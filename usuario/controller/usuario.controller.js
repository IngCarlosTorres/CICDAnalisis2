const { Respo, Requ } = require('express');
const aws_keys = require('../models/config');
const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient(aws_keys.dynamodb);
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const bcryptjs = require('bcryptjs');
const Speakeasy = require('speakeasy');
const nodemailer = require('nodemailer');

const registrarUsuario = (req = Request, res = Response) => {
    let body = req.body;
    let usuario= body.usuario;
    let nombre=body.nombre;
    let fecha_nacimiento=body.fecha_nacimiento;
    let correo =  body.correo;
    let contrasena = body.contrasena;
    let contrasena2 = body.contrasena2;
    let numero = body.numero;
    let id_user = crypto.createHash('sha1').update(usuario + contrasena).digest('hex');
    let secret = Speakeasy.generateSecret({ length: 20 });

    let contraIgual = true;
    let existe_user = false;
    let existe_correo= false;
    const esCorreoElectronico = correo=> /\S+@\S+/.test(correo);

    const esUsuario= usuario=> /^[a-zA-Z][a-zA-Z0-9-_]+$/.test(usuario);

    //var regexp = /^[a-zA-Z0-9\-\_]$/;
    var check = usuario;
    //Comprobar si ya existe Usuario y Correo
    //--Validacion de Usuario--
    let paramsScan = {
        TableName: aws_keys.aws_table_name,
        Limit: 100 
    };
    ddb.scan(paramsScan, function(err, data){
        if(err){
            console.log("Error", err);
        }  else{
              for (const usuarios of data.Items) {
                if(usuarios.usuario==usuario){
                    existe_user=true;
                    break;
                }
                if(usuarios.correo==correo){
                    existe_correo=true;
                    break;
                } 
              }
              if(existe_user){
                res.send({
                'message': 'Error: Nombre de usuario ya existe',
                });
              }else if(existe_correo){
                res.send({
                    'message': 'Error: Correo ya en uso',
                });
              }else{
                  if(esCorreoElectronico(correo)){
                      if(esUsuario(usuario)){
                        let reciclaje = {
                            "id_user": id_user,
                            "fecha_creacion": new Date().toString(),
                            "nombre": "papelera",
                            "id": uuidv4(),
                            "carpetas": [],
                            "archivos": []
                        }
                        var carpeta ={
                            "id_user":id_user,
                            "fecha_creacion": new Date().toString(),
                            "nombre":"root",
                            "id":uuidv4(),
                            "carpetas":[reciclaje],
                            "archivos":[]
                        }
                        let contraHash = bcryptjs.hashSync(contrasena, 8);             
                        var input = {
                            "id":id_user,
                            "usuario":usuario,
                            "nombre":nombre,
                            "correo":correo,
                            "contrasena":contraHash,
                            "Fecha":fecha_nacimiento,
                            "carpetas":[carpeta],
                            "id_papelera": reciclaje.id,
                            "auth": 0,
                            "numero": numero,
                            "tipo_notificacion": 0,
                            "secret": secret.base32
                        }
                                
                        var params = {
                            TableName: aws_keys.aws_table_name,
                            Item:  input
                        };
                          
                        // validar contraseña
                        (contrasena==contrasena2) ? contraIgual = true:contraIgual = false
                        if(contraIgual){
                            ddb.put(params, function (err, data) {     
                                if (err) {
                                    console.log("users::save::error - " + JSON.stringify(err, null, 2));
                                    res.send({
                                        'message': 'Error',
                                    });                   
                                } else {
                                    console.log("users::save::success" );     
                                    res.send({
                                        'message': 'Usuario Registrado con Exito',
                                        'id_user': id_user
                                    });                 
                                }
                            });
                        }else{
                            res.send({
                                'message': 'Error: Ingrese de nuevo la contraseña',
                            });  
                        }
                    }else{
                        res.send({
                            'message': 'Error: Ingrese un usuario valido',
                        }); 
                    }
                    
                  }else{
                    res.send({
                        'message': 'Error: Ingrese un correo valido',
                    }); 
                  }
                
                  
              }
  
  
        }
    });
   
    
}

const loginUsuario = (req = Request, res = Response) => {
    let body = req.body;
    let usuario= body.usuario
    let contrasena = body.contrasena
    //let id_user = crypto.createHash('sha1').update(usuario + contrasena).digest('hex');
    let existe_user =true
    let paramsScan = {
        TableName: aws_keys.aws_table_name,
        Limit: 100 
    };
    ddb.scan(paramsScan, function(err, data){
        if(err){
            console.log("Error", err);
        }  else{
              
            for (const usuarios of data.Items) {
                let compare = bcryptjs.compareSync(contrasena, usuarios.contrasena.toString())
                if(usuarios.usuario==usuario && compare){
                    existe_user=true;                    
                    console.log(usuarios);
                    res.send(usuarios);
                    break;
                }else{
                    existe_user=false;
                } 
            }
            if(!existe_user){
                res.send({
                    'message': 'Error: Usuario no encontrado',
                });
                //res.send("Error: Usuario no encontrado")
            }
        }
    });
}

const validateToken = (req = Requ, res = Respo) => {
    let body = req.body;

    let secret = body.secret;
    let token = body.token;

    let valido = Speakeasy.totp.verify({
        secret: secret,
        encoding: "base32",
        token: token,
        window : 6 //3 minutos (6 * 30 seg = 3 min)
    }); 

    res.send({
        'message': 'Token Validado',
        'token_valido': valido
    })
}

module.exports = {
    registrarUsuario,
    loginUsuario,
    validateToken
}