const express = require('express')
const routerUsuario = express.Router()
var usuarioController = require('../controller/usuario.controller')
routerUsuario.post('/crear', usuarioController.registrarUsuario);
routerUsuario.post('/login',usuarioController.loginUsuario);
routerUsuario.post('/validarToken', usuarioController.validateToken);

module.exports = routerUsuario
