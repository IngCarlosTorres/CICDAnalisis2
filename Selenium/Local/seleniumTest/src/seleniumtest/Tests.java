/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seleniumtest;

import java.util.concurrent.TimeUnit;
import net.bytebuddy.asm.Advice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.paulhammant.ngwebdriver.*;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author Torres
 */
public class Tests {
    
    private static WebDriver driver = null;
    private static NgWebDriver ngDriver = null;
    private static String pathproject =  System.getProperty("user.dir");
    private static String pathDriverChrome = pathproject+"\\extras\\geckodriver.exe";
    
    
    private void IniciarNavegador()
    {
        driver = null;
        System.setProperty("webdriver.gecko.driver",pathDriverChrome); 
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    
    public void RegistroUsuario() throws InterruptedException
    {
        System.out.println("------- REGISTRANDO USUARIO -------");
        this.IniciarNavegador();
        driver.get("http://localhost:4200/auth/register");
        driver.findElement(By.name("nombre")).sendKeys("Automatizacion"); 
        Thread.sleep(2000);
        driver.findElement(By.name("fecha-nacimiento")).sendKeys("1999-01-12");
        Thread.sleep(2000);
        driver.findElement(By.name("telefono")).sendKeys("51851736");
        Thread.sleep(2000);
        driver.findElement(By.name("correo")).sendKeys("carlostorres.usac@gmail.com");
        Thread.sleep(2000);
        driver.findElement(By.name("usuario")).sendKeys("automatize");
        Thread.sleep(2000);
        driver.findElement(By.name("contrasena")).sendKeys("1234");
        Thread.sleep(2000);
        driver.findElement(By.name("contrasena2")).sendKeys("1234");
        Thread.sleep(2000);
        WebElement check = driver.findElement(By.id("customCheckLogin"));
        check.click();
        Thread.sleep(2000);
        WebElement botonRegistrar = driver.findElement(By.id("btnRegistrar"));
        botonRegistrar.click();
        Thread.sleep(2000);
        driver.quit();
        Thread.sleep(4000);
    }
    
    public void AutenticacionFallida() throws InterruptedException
    {
        System.out.println("------- AUTENTICACIÓN DE USUARIO FALLIDO-------");
        this.IniciarNavegador();
        driver.get("http://localhost:4200/auth/login"); 
        driver.findElement(By.name("NombreUsuario")).sendKeys("UsuarioFallido");
        Thread.sleep(2000);
        driver.findElement(By.name("Password")).sendKeys("1234");
        Thread.sleep(2000);
        WebElement botonLogin = driver.findElement(By.id("btnLogin"));
        botonLogin.click();
        Thread.sleep(2000);
        driver.quit();
        Thread.sleep(4000);
    }
    
    public void AutenticacionExitosa() throws InterruptedException
    {
        System.out.println("------- AUTENTICACIÓN DE USUARIO EXITOSO-------");
        this.IniciarNavegador();
        driver.get("http://localhost:4200/auth/login");
        driver.findElement(By.name("NombreUsuario")).sendKeys("automatize");
        Thread.sleep(2000);
        driver.findElement(By.name("Password")).sendKeys("1234");
        Thread.sleep(2000);
        WebElement botonLogin = driver.findElement(By.id("btnLogin"));
        botonLogin.click();
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("OK")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("cerrarSesion")).click();
        Thread.sleep(2000);
        driver.quit();
        Thread.sleep(4000);
    }
    
    public void CrearCarpeta() throws InterruptedException
    {
        System.out.println("------- CREACIÓN DE CARPETA -------");
        this.IniciarNavegador();
        driver.get("http://localhost:4200/auth/login");
        JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
        NgWebDriver ngDriver = new NgWebDriver(jsDriver);
        ngDriver.waitForAngularRequestsToFinish();
        driver.findElement(By.name("NombreUsuario")).sendKeys("automatize");
        Thread.sleep(2000);
        driver.findElement(By.name("Password")).sendKeys("1234");
        Thread.sleep(2000);
        WebElement botonLogin = driver.findElement(By.id("btnLogin"));
        botonLogin.click();
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("OK")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("subirCarpeta")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("swal2-input")).sendKeys("Selenium");
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("OK")).click();
        Thread.sleep(4000);
        driver.findElement(By.id("cerrarSesion")).click();
        Thread.sleep(3000);
        driver.quit();
        Thread.sleep(4000);
    }
    
    public void EliminarCarpeta() throws InterruptedException
    {
        System.out.println("------- CREACIÓN DE CARPETA -------");
        this.IniciarNavegador();
        driver.get("http://localhost:4200/auth/login");
        JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
        NgWebDriver ngDriver = new NgWebDriver(jsDriver);
        ngDriver.waitForAngularRequestsToFinish();
        driver.findElement(By.name("NombreUsuario")).sendKeys("automatize");
        Thread.sleep(2000);
        driver.findElement(By.name("Password")).sendKeys("1234");
        Thread.sleep(2000);
        WebElement botonLogin = driver.findElement(By.id("btnLogin"));
        botonLogin.click();
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("OK")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("eliminarCarpeta")).click();
        Thread.sleep(2000);
        driver.findElement(By.id("swal2-input")).sendKeys("Selenium");
        Thread.sleep(2000);
        driver.findElement(ByAngular.buttonText("OK")).click();
        Thread.sleep(4000);
        driver.findElement(By.id("cerrarSesion")).click();
        Thread.sleep(3000);
        driver.quit();
        Thread.sleep(4000);
    }
}
