FROM node:latest

WORKDIR /usr/src/app

COPY archivo/package*.json ./archivo/
RUN cd ./archivo && npm install
COPY archivo/ ./archivo

COPY microservice-carpeta/package*.json ./microservice-carpeta/
RUN cd ./microservice-carpeta && npm install
COPY microservice-carpeta/ ./microservice-carpeta

COPY microservice-usuario/package*.json ./microservice-usuario/
RUN cd ./microservice-usuario && npm install
COPY microservice-usuario/ ./microservice-usuario

COPY microservicio-backlog/package*.json ./microservicio-backlog/
RUN cd ./microservicio-backlog && npm install
COPY microservicio-backlog/ ./microservicio-backlog

COPY middleware/package*.json ./middleware/
RUN cd ./middleware && npm install
COPY middleware/ ./middleware


COPY ./script-up-back.sh .
RUN chmod +x ./script-up-back.sh

EXPOSE 3000
CMD ./script-up-back.sh