const { Builder, By, Key, until } = require("selenium-webdriver");
const { expect } = require('chai');
const assert = require('assert');
const { isTypedArray } = require("util/types");

describe("-- EJECUTANDO TEST CON SELENIUM GRIND, MOCHA Y CHAI --", () => {

  let datos = {
    nombre: "SeleniunGrid",
    fecha_nacimiento: "1999-01-12",
    telefono: "45823698",
    correo: "seleniumgrid@gmail.com",
    usuario: "seleniumgrid",
    contrasena: "123",
    contrasena2: "123",
    usuariofallido: "sele",
    carpeta: "SeleniumGrid"
  };
  try {
    let driver = await new Builder().forBrowser('firefox').usingServer('http://192.168.1.9:4444/wd/hub').build();
    
    it('-- EMPEZANDO PRUEBAS FUNCINALES --',function(done))
    {
      driver.get('http://localhost:4200/auth/register')
      .then(async function () {
        console.log("--- TEST REGISTRAR USUARIO ---")
        console.log("--- Cargando pagina Registrar Usuario ---")
        let findTimeOutP = driver.sleep(10000);
        return findTimeOutP;
      })
      .then(async function () {
        console.log("--- Registrando Datos ---");
        var nombre = await driver.findElement(By.name("nombre"));
        var fecha_nac = await driver.findElement(By.name("fecha-nacimiento"));
        var telefono = await driver.findElement(By.name("telefono"));
        var correo = await driver.findElement(By.name("correo"));
        var usuario = await driver.findElement(By.name("usuario"));
        var contrasena = await driver.findElement(By.name("contrasena"));
        var contrasena2 = await driver.findElement(By.name("contrasena2"));
        var check = await driver.findElement(By.id("customCheckLogin"));

        await nombre.sendKeys(datos.nombre);
        await fecha_nac.sendKeys(datos.fecha_nacimiento);
        await telefono.sendKeys(datos.telefono);
        await correo.sendKeys(datos.correo);
        await usuario.sendKeys(datos.usuario);
        await contrasena.sendKeys(datos.contrasena);
        await contrasena2.sendKeys(datos.contrasena2);
        await check.click().then(function () { driver.sleep(2000) });
        return "result";
      })
      .then(async function () {
        console.log("--- PAUSA PARA MOSTRAR DATOS INGRESADOS ---");
        let findTimeOutP = driver.sleep(3000);
        return findTimeOutP;
      })
      .then(async function () {
        console.log("--- PRESIONANDO EL BOTON REGISTRAR ---");
        var button_registro = await driver.findElement(By.id("btnRegistrar"));
        await button_registro.click().then(function () { driver.sleep(2000) });
        return "result"
      })
      .then(async function () {
        console.log("--- VISUALIZANDO MENSAJE DE REGISTRO ---");
        let findTimeOutP = driver.sleep(4000);
        return findTimeOutP;
      })
    }
  } catch (error) {
    console.log(error);
  }
})




async function executeFirebox(datos) {

}


executeFirebox(datos);
