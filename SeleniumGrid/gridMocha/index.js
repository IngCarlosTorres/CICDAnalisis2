const { Builder, By, Key, until } = require("selenium-webdriver");
const { expect } = require('chai');
const assert = require('assert');

/*
describe("-- EJECUTANDO TEST CON SELENIUM GRIND, MOCHA Y CHAI --", () => {

    let datos = {
        nombre: "SeleniunGrid",
        fecha_nacimiento: "1999-01-12",
        telefono: "45823698",
        correo: "seleniumgrid@gmail.com",
        usuario: "seleniumgrid",
        contrasena: "123",
        contrasena2: "123",
        usuariofallido: "sele",
        carpeta: "SeleniumGrid"
    };

    let driver = new Builder().forBrowser('firefox').usingServer('http://192.168.174.1:4444/wd/hub').build();

    it('-- EMPEZANDO PRUEBAS FUNCINALES --', function (done) {
        driver.get('http://localhost:4200/auth/register')
            .then(async function () {
                console.log("--- TEST REGISTRAR USUARIO ---")
                console.log("--- Cargando pagina Registrar Usuario ---")
                let findTimeOutP = driver.sleep(10000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("--- Registrando Datos ---");
                var nombre = await driver.findElement(By.name("nombre"));
                var fecha_nac = await driver.findElement(By.name("fecha-nacimiento"));
                var telefono = await driver.findElement(By.name("telefono"));
                var correo = await driver.findElement(By.name("correo"));
                var usuario = await driver.findElement(By.name("usuario"));
                var contrasena = await driver.findElement(By.name("contrasena"));
                var contrasena2 = await driver.findElement(By.name("contrasena2"));
                var check = await driver.findElement(By.id("customCheckLogin"));

                await nombre.sendKeys(datos.nombre);
                await fecha_nac.sendKeys(datos.fecha_nacimiento);
                await telefono.sendKeys(datos.telefono);
                await correo.sendKeys(datos.correo);
                await usuario.sendKeys(datos.usuario);
                await contrasena.sendKeys(datos.contrasena);
                await contrasena2.sendKeys(datos.contrasena2);
                await check.click().then(function () { driver.sleep(2000) });
                return "result";
            })
            .then(async function () {
                console.log("--- PAUSA PARA MOSTRAR DATOS INGRESADOS ---");
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("--- PRESIONANDO EL BOTON REGISTRAR ---");
                var button_registro = await driver.findElement(By.id("btnRegistrar"));
                await button_registro.click().then(function () { driver.sleep(2000) });
                return "result"
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE REGISTRO ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- REDIRIGIENDO A LOGIN--");   
                var botonRegresar = await driver.findElement(By.id("btnRegresar"));
                await botonRegresar.click().then(function () { driver.sleep(2000) }); 
                var botonLogin = await driver.findElement(By.id("btnLogin"));
                await botonLogin.click().then(function () { driver.sleep(2000) }); 
                return "result";
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- INGRESANDO CREDENCIALES FALLIDAS--");   
                var user =await driver.findElement(By.name("NombreUsuario"));
                var password =await driver.findElement(By.name("Password"));
                await user.sendKeys("userFail"); //USUARIO QUE SE VA LOGUEAR
                await password.sendKeys("123");
                var botonLogin = await driver.findElement(By.id("btnLogin"));
                await botonLogin.click().then(function () { driver.sleep(2000) });
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE LOGIN FALLIDO ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- INGRESANDO CREDENCIALES CORRECTAS--");   
                var user =await driver.findElement(By.name("NombreUsuario"));
                var password =await driver.findElement(By.name("Password"));
                await user.sendKeys(datos.nombre); //USUARIO QUE SE VA LOGUEAR
                await password.sendKeys(datos.contrasena);
                var botonLogin = await driver.findElement(By.id("btnLogin"));
                await botonLogin.click().then(function () { driver.sleep(2000) });
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE LOGIN EXITOSO ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- CREANDO CARPETA--");   
                var btnFolder =await driver.findElement(By.id("subirCarpeta"));
                await btnFolder.click().then(function () { driver.sleep(2000) });
                var folder =await driver.findElement(By.id("swal2-input"));
                await folder.sendKeys(datos.carpeta);
                driver.switchTo().alert().accept();
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE CARPETA CREADA ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- ELMINANDO CARPETA--");   
                var btnFolder =await driver.findElement(By.id("eliminarCarpeta"));
                await btnFolder.click().then(function () { driver.sleep(2000) });
                var folder =await driver.findElement(By.id("swal2-input"));
                await folder.sendKeys(datos.carpeta);
                driver.switchTo().alert().accept();
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE CARPETA ELIMINADA ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function () {
                console.log("SUCCESS");
                let findTimeOutP = driver.sleep(3000);
                expect('success').to.equal('success');
                done();
            })
            .then(async function () {
                console.log("-- CERRANDO NAVEGADOR --");
                driver.quit();
            });;
    })
})
*/

describe("-- EJECUTANDO TEST CON SELENIUM GRIND, MOCHA Y CHAI --", () => {

    let datos = {
        nombre: "SeleniunGrid",
        fecha_nacimiento: "1999-01-12",
        telefono: "45823698",
        correo: "seleniumgrid@gmail.com",
        usuario: "seleniumgrid",
        contrasena: "123",
        contrasena2: "123",
        usuariofallido: "sele",
        carpeta: "SeleniumGrid"
    };

    let driver = new Builder().forBrowser('firefox').usingServer('http://192.168.174.1:4444/wd/hub').build();

    it('-- EMPEZANDO PRUEBAS FUNCINALES --', function (done) {
        driver.get('http://localhost:4200/auth/login')
            .then(async function () {
                console.log("--- TEST USUARIO ---")
                console.log("--- Cargando pagina Registrar Usuario ---")
                let findTimeOutP = driver.sleep(10000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- INGRESANDO CREDENCIALES FALLIDAS--");   
                var user =await driver.findElement(By.name("NombreUsuario"));
                var password =await driver.findElement(By.name("Password"));
                await user.sendKeys("userFail"); //USUARIO QUE SE VA LOGUEAR
                await password.sendKeys("123");
                var botonLogin = await driver.findElement(By.id("btnLogin"));
                await botonLogin.click().then(function () { driver.sleep(2000) });
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE LOGIN FALLIDO ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- INGRESANDO CREDENCIALES CORRECTAS--");   
                var user =await driver.findElement(By.name("NombreUsuario"));
                var password =await driver.findElement(By.name("Password"));
                await user.sendKeys(datos.nombre); //USUARIO QUE SE VA LOGUEAR
                await password.sendKeys(datos.contrasena);
                var botonLogin = await driver.findElement(By.id("btnLogin"));
                await botonLogin.click().then(function () { driver.sleep(2000) });
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE LOGIN EXITOSO ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- CREANDO CARPETA--");   
                var btnFolder =await driver.findElement(By.id("subirCarpeta"));
                await btnFolder.click().then(function () { driver.sleep(2000) });
                var folder =await driver.findElement(By.id("swal2-input"));
                await folder.sendKeys(datos.carpeta);
                driver.switchTo().alert().accept();
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE CARPETA CREADA ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function(){
                console.log("-- ELMINANDO CARPETA--");   
                var btnFolder =await driver.findElement(By.id("eliminarCarpeta"));
                await btnFolder.click().then(function () { driver.sleep(2000) });
                var folder =await driver.findElement(By.id("swal2-input"));
                await folder.sendKeys(datos.carpeta);
                driver.switchTo().alert().accept();
                return "result";
            })
            .then(async function () {
                console.log("--- VISUALIZANDO MENSAJE DE CARPETA ELIMINADA ---");
                let findTimeOutP = driver.sleep(4000);
                return findTimeOutP;
            })
            .then(async function () {
                console.log("-- CERRANDO ALERT --");
                driver.switchTo().alert().accept();
            })
            .then(async function(){
                console.log("-- EMPEZANDO A REFRESCAR PAGINA --");    
                let findTimeOutP = driver.sleep(3000);
                return findTimeOutP;
            })
            .then(async function(){
                console.log("-- REFRESCANDO PAGINA --");
                driver.navigate().refresh(); 
            })
            .then(async function () {
                console.log("SUCCESS");
                let findTimeOutP = driver.sleep(3000);
                expect('success').to.equal('success');
                done();
            })
            .then(async function () {
                console.log("-- CERRANDO NAVEGADOR --");
                driver.quit();
            });;
    })
})
  