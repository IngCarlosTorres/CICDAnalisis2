const { Router } = require('express');
const { crearCarpeta, borrarCarpeta, rootCarpeta, verCarpeta, actualizarCarpeta,  } = require('../controller/carpeta');
const router = Router();

// RUTA PARA CREAR CARPETA
router.post('/create', crearCarpeta);

// RUTA PARA BORRAR CARPETA
router.post('/delete', borrarCarpeta);

// RUTA PARA VER CARPETA
router.post('/read', verCarpeta);

// RUTA PARA OBTENER RAÍZ 
router.post('/root', rootCarpeta);

// RUTA PARA ACTUALIZAR CARPETA
router.post('/update', actualizarCarpeta); 

module.exports = router;
