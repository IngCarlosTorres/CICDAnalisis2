const { Response, Request } = require('express');
const aws_keys = require('../models/creds');
const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient(aws_keys.dynamodb);
const s3 = new AWS.S3(aws_keys.s3);
var nodemailer = require('nodemailer'); 
const { v4: uuidv4 } = require('uuid');
//const Speakeasy = require('speakeasy');

var tipo=-1;
var archivo=[];

const crearArchivo = async (req = Request, res = Response)  => {
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let nombre_archivo=body.nombre_archivo;
    let ext=body.ext;
    let fecha_subida=new Date(Date.now()).toUTCString();
    let archivo=body.archivo

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let link="https://aydarchivos.s3.amazonaws.com/"+"user_"+id_user+"/"+nombre_archivo+"."+ext;
                let input = {
                    "id_archivo": uuidv4(),
                    "nombre_archivo":nombre_archivo,
                    "extension":ext,
                    "fecha_subida":fecha_subida,
                    "link":link,
                    "id_carpeta": id_carpeta,
                    "cuerpo":archivo
                }
                let datos={
                    nombre_archivo:nombre_archivo,
                    id_user:id_user,
                    ext:ext,
                    fecha_subida:fecha_subida,
                    archivo:archivo
                }
                tipo = 0;
                estado = buscar(data.Item, id_carpeta,datos,input);
                if(estado === 200){
                    let url = "user_"+datos.id_user+"/"+datos.nombre_archivo+"."+datos.ext;
                    let content="";
              
                    /**Validacion extension */
                    switch(ext){
                        case "csv":
                            content="text/csv";
                            break;
                        case "csv1":
                            content="application/csv";
                            break;
                        case "json":
                            content="application/json";
                            break;
                        case "xlsx":
                            content="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            break;
                        case "xlsb":
                            content="application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                            break;
                        case "xls":
                            content="application/vnd.ms-excel";
                            break;
                        case "xlsm":
                            content="application/vnd.ms-excel.sheet.macroEnabled.12";
                            break;
                        case "gif":
                            content="image/gif";
                            break;
                        case "bmp":
                            content="image/bmp";
                            break;
                        case "jpg":
                            content="image/jpeg";
                            break;
                        case "jpeg":
                            content="image/jpeg";
                            break;
                        case "png":
                            content="image/png";
                            break;
                        case "doc":
                            content="application/msword";
                            break;
                        case "docm":
                            content="application/vnd.ms-word.document.macroEnabled.12";
                            break;
                        case "docx":
                            content="application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                            break;
                        case "dotx":
                            content="application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                            break;
                        case "dotm":
                            content="application/vnd.ms-word.template.macroEnabled.12";
                            break;
                        case "html":
                            content="text/html";
                            break;
                        case "pdf":
                            content="application/pdf";
                            break;
                        case "potm":
                            content="application/vnd.ms-powerpoint.template.macroEnabled.12";
                            break;
                        case "potx":
                            content="application/vnd.openxmlformats-officedocument.presentationml.template";
                            break;
                        case "ppam":
                            content="application/vnd.ms-powerpoint.addin.macroEnabled.12";
                            break;
                        case "pps":
                            content="application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                            break;
                        case "ppsx":
                            content="application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                            break;
                        case "ppsm":
                            content="application/vnd.ms-powerpoint.slideshow.macroEnabled.12";
                            break;
                        case "ppt":
                            content="application/vnd.ms-powerpoint";
                            break;
                        case "pptm":
                            content="application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                            break;
                        case "pptx":
                            content="application/vnd.openxmlformats-officedocument.presentationml.presentation";
                            break;
                        case "rtf":
                            content="application/rtf";
                            break;
                        case "txt":
                            content="text/plain";
                            break;
                        case "avi":
                            content="video/x-msvideo";
                            break;
                        case "aac":
                            content="audio/aac";
                            break;
                        case "mpeg":
                            content="video/mpeg";
                            break;
                        case "mp3":
                            content="video/mpeg";
                            break;
                        case "ico":
                            content="image/x-icon";
                            break;
                        case "oga":
                            content="audio/ogg";
                            break;
                        case "ogv":
                            content="video/ogg";
                            break;
                        case "wav":
                            content="audio/x-wav";
                            break;
                        case "mp4":
                            content="video/mp4";
                            break;
                        default:
                            console.log('Error! extension');
                            res.send({
                                'message': 'Error, Archivo con extension peligrosa',
                                'estado': 405
                            });
                    }
                    let subir_archivo = await subirS3(archivo,url,content);
                   
                    ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": data.Item.carpetas
                        }
                    }, function(err, data){
                        if (err) {
                            console.log('Error al crear el archivo: ', err);
                            res.send({
                                'message': 'Error al actualizar la informacion del archivo en la base de datos!',
                                'estado': 400
                            });
                        } else {
                            console.log('Archivo creado!');
                            res.send({
                                'message': 'Archivo creado',
                                'estado': 200
                            });
                        }
                    })
                }else if(estado == 401){
                    res.send({
                        'message':'Ya existe un archivo con ese nombre y extension',
                        'archivo': nombre_archivo+"."+ext,
                        'estado':401
                    });
                }else if(estado == 404){
                    res.send({
                        'message': 'Error al localizar la carpeta',
                        'archivo': nombre_archivo+"."+ext,
                        'estado': 404
                    });
                }
            }            
        }
    });
}

const verArchivo = async (req = Request, res = Response) =>{
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo
                }
                tipo = 1;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    res.send({
                        'message': 'Archivo encontrado!',
                        'archivo' : estado,
                        'estado': 200
                    })
                }

            }
        }
    })
}

const editarArchivo = async (req = Request, res = Response) =>{ 
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;
    let nombre_nuevo=body.nombre;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo,
                    nombre_nuevo:nombre_nuevo
                }
                tipo = 2;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": data.Item.carpetas
                        }
                    }, function(err, data){
                        if (err) {
                            console.log('Error al crear el archivo: ', err);
                            res.send({
                                'message': 'Error al actualizar la informacion del archivo en la base de datos!',
                                'estado': 402
                            });
                        } else {
                            res.send({
                                'message': 'Archivo modificado!',
                                'archivo' : estado,
                                'estado': 200
                            })
                        }
                    })
                    
                }

            }
        }
    })
}

const eliminarArchivo = async (req = Request, res = Response) =>{ 
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo
                }
                //elimino de s3
                tipo = 5;
                estado = buscar(data.Item, id_carpeta,datos,{});
             
                if(estado == 405){
                    res.send({
                        'message': 'Error al obtener la informacion del archivo',
                        'estado': 405
                    });
                }else{
                    let direccion="user_"+id_user+"/"+estado.nombre_archivo+"."+estado.extension;
                    let eliminars3=await deleteS3(direccion);

                    //Eliminando en la bd
                    let datos={
                        id_archivo:id_archivo
                    }
                    tipo = 3;
                    estado = buscar(data.Item, id_carpeta,datos,{});
                    if(estado == 401){
                        res.send({
                            'message': 'Error, el archivo no encontrado',
                            'estado': 401
                        })
                    }else{
                        ddb.update({
                            TableName: "aydrive",
                            Key: {
                                id: id_user
                            },
                            UpdateExpression: "SET #carpetas = :carpetas",
                            ExpressionAttributeNames: { "#carpetas": "carpetas" },
                            ExpressionAttributeValues: {
                                ":carpetas": data.Item.carpetas
    
                            }}, async function(err, data){
                                if (err) {
                                    console.log('Error al crear el archivo: ', err);
                                    res.send({
                                        'message': 'Error al actualizar la informacion del archivo en la base de datos!',
                                        'estado': 400
                                    });
                                } else {
                                    res.send({
                                        'message': 'Archivo Eliminado!',
                                        'estado': 200
                                    });
                                    
                                }
                        });
                    }
                    
                }
            }
        }
    })
}

const verIdArchivo= async (req = Request, res = Response) =>{
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let nombre_archivo=body.nombre;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    nombre_archivo:nombre_archivo
                }
                tipo = 4;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    res.send({
                        'message': 'Archivo encontrado!',
                        'archivo' : estado,
                        'estado': 200
                    })
                }

            }
        }
    })
}

const moverArchivo = async (req = Request, res = Response) =>{ 
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;
    let id_carpeta_nueva = body.id_carpeta_nueva;
    archivo=[];

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo,
                    id_carpeta:id_carpeta,
                    id_carpeta_nueva:id_carpeta_nueva
                }
                tipo = 3;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    console.log(archivo);
                    archivo.restaurar = id_carpeta_nueva;
                    tipo = 0;
                    estado2 = buscar(data.Item, id_carpeta_nueva,archivo,archivo);
                    if(estado2 == 401){
                        res.send({
                            'message': 'Error, ya existe un archivo con ese nombre en esa carpeta',
                            'estado': 401
                        })
                    }else{
                        //actualizo
                        ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": data.Item.carpetas
                        }
                        }, function(err, data){
                            if (err) {
                                console.log('Error al crear el archivo: ', err);
                                res.send({
                                    'message': 'Error al actualizar la informacion del archivo en la base de datos!',
                                    'estado': 402
                                });
                            } else {
                                res.send({
                                    'message': 'Archivo movido!',
                                    'estado': 200
                                })
                            }
                        })

                    }
                }

            }
        }
    })
}

const moverPapelera = async (req = Request, res = Response) =>{ 
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;
    let id_carpeta_nueva = body.id_carpeta_nueva;
    archivo=[];

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo,
                    id_carpeta:id_carpeta,
                    id_carpeta_nueva:id_carpeta_nueva
                }
                tipo = 3;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    console.log(archivo);
                    archivo.restaurar = id_carpeta_nueva;
                    tipo = 0;
                    estado2 = buscar(data.Item, id_carpeta_nueva,archivo,archivo);
                    if(estado2 == 401){
                        res.send({
                            'message': 'Error, ya existe un archivo con ese nombre en esa carpeta',
                            'estado': 401
                        })
                    }else{
                        //actualizo
                        ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": data.Item.carpetas
                        }
                        }, function(err, data){
                            if (err) {
                                console.log('Error al crear el archivo: ', err);
                                res.send({
                                    'message': 'Error al actualizar la informacion del archivo en la base de datos!',
                                    'estado': 402
                                });
                            } else {
                                res.send({
                                    'message': 'Archivo movido!',
                                    'estado': 200
                                })
                            }
                        })

                    }
                }

            }
        }
    })
}

const enviarArchivos =  async(req = Request, res = Response) => {
    let body = req.body;
    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let id_archivo=body.id_archivo;
    let correo=body.correo;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, async function(err, data){
        if (err) {
            console.log('Error al leer el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{ 
                let datos={
                    id_archivo:id_archivo
                }
                tipo = 1;
                estado = buscar(data.Item, id_carpeta,datos,{});
                if(estado == 401){
                    res.send({
                        'message': 'Error, el archivo no encontrado',
                        'estado': 401
                    })
                }else{
                    //si el archivo fue localizado lo mando por correo
                    if(estado.extension == "txt"){
                        let name=estado.nombre_archivo+"."+estado.extension;
                        if(estado.cuerpo == ""){
                            let mail = enviarEmail(correo,name,estado.link,0,s);
                            if(mail == 500){
                                //console.log('houston we have a problemo.....')
                                res.send({
                                    'message': 'Ocurrio un error al enviar el email',
                                    'archivo' : estado,
                                    'estado': 200
                                })
                            }else{
                                res.send({
                                    'message': 'Archivo enviado!',
                                    'archivo' : estado,
                                    'estado': 200
                                })
                            }
                        }
                        var b = Buffer.from(estado.cuerpo, 'base64')
                        var s = b.toString();
                        let mail = enviarEmail(correo,name,estado.link,1,s);
                        if(mail == 500){
                            //console.log('houston we have a problemo.....')
                            res.send({
                                'message': 'Ocurrio un error al enviar el email',
                                'archivo' : estado,
                                'estado': 200
                            })
                        }else{
                            res.send({
                                'message': 'Archivo enviado!',
                                'archivo' : estado,
                                'estado': 200
                            })
                        }
                    }else{
                        let name=estado.nombre_archivo+"."+estado.extension;
                        let mail = enviarEmail(correo,name,estado.link,0,"");
                  
                        if(mail == 500){
                            //console.log('houston we have a problemo.....')
                            res.send({
                                'message': 'Ocurrio un error al enviar el email',
                                'archivo' : estado,
                                'estado': 200
                            })
                        }else{
                            res.send({
                                'message': 'Archivo enviado!',
                                'archivo' : estado,
                                'estado': 200
                            })
                        }
                    }
                    
                }

            }
        }
    })
}

function buscar(arreglo, id, datos,new_file){
    if(arreglo.carpetas) {
        for (let i in arreglo.carpetas)  {
            if (arreglo.carpetas[i].id == id){
                if(arreglo.carpetas[i].archivos){
                    if(tipo == 0){//crear archivo
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].nombre_archivo === datos.nombre_archivo
                                && arreglo.carpetas[i].archivos[j].extension === datos.ext){
                                return 401;
                            }
                        }
                        arreglo.carpetas[i].archivos.push(new_file);
                        return 200;
                    
                    }else if(tipo == 1){//ver archivo
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].id_archivo == datos.id_archivo){
                                return arreglo.carpetas[i].archivos[j];
                            }
                        }
                        return 401;
                        
                    }else if(tipo == 2){// editar archivo
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].id_archivo == datos.id_archivo){
                                arreglo.carpetas[i].archivos[j].nombre_archivo = datos.nombre_nuevo;
                                return arreglo.carpetas[i].archivos[j];
                            }
                        }
                        return 401;
                    }else if(tipo == 3){// eliminar archivo
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].id_archivo == datos.id_archivo){
                                archivo=arreglo.carpetas[i].archivos[j];//para mover archivo
                                delete arreglo.carpetas[i].archivos[j];
                                return 200;
                            }
                        }
                        return 401;
                    }else if(tipo == 4){//obtener el id de un archivo
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].nombre_archivo == datos.nombre_archivo){
                                return arreglo.carpetas[i].archivos[j];
                            }
                        }
                        return 401;
                    }else if(tipo == 5){//me devuelve los datos
                        for(let j=0; j < arreglo.carpetas[i].archivos.length;j++){
                            if(arreglo.carpetas[i].archivos[j].id_archivo == datos.id_archivo){
                                return arreglo.carpetas[i].archivos[j];
                            }
                        }
                        return 405;
                    }
                }
            }else if(arreglo.carpetas[i].carpetas.length){
                return buscar(arreglo.carpetas[i], id, datos, new_file);
            }
        }
    }

    return 404;
}

async function subirS3 (base64, direccion, content){
    let buffer = new Buffer.from(base64,'base64')

    const params = {
      Bucket: "aydarchivos",
      Key: direccion,
      Body: buffer,
      ACL:'public-read',
      ContentEncoding: 'base64',
      ContentType: content
    };
    s3.upload(params, function(err, data) {
        if (err) {
          console.log("Error al subir el archivo al S3 ",err)
          return 402;
        }
        console.log("subir S3 ok");
        return 200;
    });
}

async function deleteS3 (direccion){
    var params = {
        Bucket: "aydarchivos", 
        Delete: {
         Objects: [
            {
                Key: direccion
            }
         ], 
         Quiet: false
        }
    };
    s3.deleteObjects(params, function(err, data) {
        if (err) {
            console.log(err); 
            return 400;
        }else {
            console.log("Archivo eliminado en S3!");  
            return 200;
        }             // successful response
        
    });
}

function enviarEmail(correo,filename,path,tipo,cuerpo){
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'dcmc2297@gmail.com',
            pass: 'IL0veC4ts.22'
        }
    });

    let cuerpo_correo="<!DOCTYPE html>"+ 
    "<body style=\"margin:0;padding:0;word-spacing:normal;background-color:#939297;\">"+
    "<div role=\"article\" aria-roledescription=\"email\"  style=\"text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#939297;\">"+
    "<table role=\"presentation\" style=\"width:100%;border:none;border-spacing:0;\"><tr><td align=\"center\" style=\"padding:0;\">"+
    "<table role=\"presentation\" style=\"width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;\">"+
    "<tr><td style=\"padding:30px;background-color:#ffffff;\"><h1 style=\"margin-top:0;margin-bottom:16px;font-size:26px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;\">AyDrive Team!</h1>"+
    "<p style=\"margin:0;\"><br><br><b>Recibiste un archivo adjunto! con el contenido: </b> \n";
    if(tipo == 1){
        cuerpo_correo+=cuerpo
    }
    cuerpo_correo+="  </p></td></tr><tr>"+
    "<td style=\"padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);\">"+
    "<div class=\"col-lge\" style=\"display:inline-block;width:100%;max-width:395px;vertical-align:top;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;\">"+
    "<p style=\"margin-top:0;margin-bottom:12px;\">For more information please:<a href=\"#\">contact us!</a> </p></div></div>"+
    "</td></tr></table></td></tr></table></div></body></html>";

    var mailOptions = {
        from: 'AyDrive Team',
        to: correo,
        subject: "Recepcion de Archivos",
        html: cuerpo_correo,
        attachments: [{'filename': filename, 'path': path}]
        
    };

        // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        if (error){
            console.log(error);
            return 500;
        } else {
            console.log("Email sent");
            //res.status(200).jsonp(req.body);
            return 200;
        }
    });
}

module.exports = {
    crearArchivo,
    verArchivo,
    editarArchivo,
    eliminarArchivo,
    verIdArchivo,
    moverArchivo,
    moverPapelera,
    enviarArchivos
}
