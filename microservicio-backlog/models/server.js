// LIBRERIAS PARA EL MANEJO DEL SERVER
const express = require('express');
const cors = require('cors');
var routerUsuario = require('../routes/mid.route')

class Server {
    
    constructor(){
        // CONFIGURACIÓN GENERAL DEL SERVIDOR
        this.app = express();
        this.port = 4000;
        this.carpetaPath = "/mid";
        
        this.app.use(cors())
        this.app.use(express.json({limit:'5mb'}))
        this.app.use(express.urlencoded({extended:false, limit:'5mb'}))

        // RUTAS DEL SERVER
        this.routes();
    }

    routes(){
       this.app.use(this.carpetaPath, routerUsuario);
    }

    listen(){
        this.app.listen( this.port, () => {
            console.log( 'Registro mid corriendo en puerto: ', this.port );
        });
    }

}

module.exports = Server;