const express = require('express')
const routerUsuario = express.Router()
var usuarioController = require('../controller/mid.controller')
routerUsuario.post('/usuarios', usuarioController.registroUsuario);
routerUsuario.post('/carpetas', usuarioController.registroCarpeta);
routerUsuario.post('/archivos', usuarioController.registroArchivos);
routerUsuario.get('/logs', usuarioController.verRegistros);

module.exports = routerUsuario
