import { Component, OnInit } from '@angular/core';
import { ArchivoService } from '../../services/archivo.service'

import Swal from 'sweetalert2'

// IMPORTACIÓN DEL SERVICIO PETICIONES
import { PeticionesService } from '../../services/peticiones.service';
import { Router, RouterLink } from '@angular/router';
import { TarjetaDesplegable } from './TarjetaDesplegable'


@Component({
  selector: 'app-root-side-var',
  templateUrl: './root-side-var.component.html'
})
export class RootSideVarComponent implements OnInit {


  collapseShow = "hidden";
  cortarPegar = "";

  archivoActual: any = {
    extension: "",
    fecha_subida: "",
    id_archivo: "",
    link: "",
    nombre_archivo: ""
  };

  private tarjetaDesplegable: TarjetaDesplegable;


  constructor(private archivoService: ArchivoService, private router: Router, private ServicePeticiones: PeticionesService) { 
    this.tarjetaDesplegable = new TarjetaDesplegable();
  }

  ngOnInit(): void {
    if (localStorage.getItem("cortarPegar") == null) {
      localStorage.setItem("cortarPegar", "cortar");
    }
    this.cortarPegar = localStorage.getItem("cortarPegar");

  }

  toggleCollapseShow(classes) {
    this.collapseShow = classes;
  }

  async deleteFile() {
    const file = JSON.parse(localStorage.getItem("currentFile"));

    file.ruta = "rmfile";

    await this.archivoService.deleteFile(file).toPromise().then(
      res => {
        console.log(res);
        localStorage.setItem('currentFile', "");
        const contenido = {
          position: 'center',
          title: "El archivo ha sido eliminado  con exito",
          showConfirmButton: false,
          timer: 2000
        }
        this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
        this.ServicePeticiones.actualizarPagina();
        this.ServicePeticiones.actualizarPagina();
        //window.location.reload();
      },
      err => {
        console.error(err);
      }
    );
  }

  async moverPapelera() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const file = JSON.parse(localStorage.getItem("currentFile"));


    console.log("id de papelera: " + user.id_papelera);
    var dataroot = {
      ruta: "papelera",
      id_user: user.id_user,
      id_carpeta: file.id_carpeta,
      id_archivo: file.id_archivo,
      id_carpeta_nueva: user.id_papelera

    };
    console.log(dataroot)
    await this.archivoService.moveFile(dataroot).toPromise().then(
      res => {
        console.log(res);
        const contenido = {
          position: 'center',
          title: "El archivo ha sido enviado a la papelera con exito",
          showConfirmButton: false,
          timer: 3000
        }
        this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
        this.ServicePeticiones.actualizarPagina();
        localStorage.setItem("oldCarpeta", file.id_carpeta);

        this.ServicePeticiones.actualizarPagina();

      },
      err => {
        console.error(err);
      }
    )

  }

  async enviar(){
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const file = JSON.parse(localStorage.getItem("currentFile"));

    var dataroot = {
      ruta: "enviar",
      id_user: user.id_user,
      id_carpeta: file.id_carpeta,
      id_archivo: file.id_archivo,
      correo: user.correo
    };

    console.log(dataroot);
    await this.archivoService.enviar(dataroot).toPromise().then(
      res => {
        console.log(res);
        const contenido = {
          position: 'center',
          title: "El archivo ha sido enviado a tu correo con exito",
          showConfirmButton: false,
          timer: 3000
        }
        this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
        this.ServicePeticiones.actualizarPagina();
        this.ServicePeticiones.actualizarPagina();

      },
      err => {
        console.error(err);
      }
    )

  }

  async moveFile() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    console.log(user)
    if (this.cortarPegar == "cortar") {
      this.cortarPegar = "pegar";
      const file = JSON.parse(localStorage.getItem("currentFile"));
      file.id_carpeta_nueva = "";
      localStorage.setItem("cortarPegar", this.cortarPegar);
      localStorage.setItem("moveFile", JSON.stringify(file));
      console.log(file);
    }
    else if (this.cortarPegar == "pegar") {
      this.cortarPegar = "cortar";
      const folder = JSON.parse(localStorage.getItem("currentFolder"));
      const newFolder = JSON.parse(localStorage.getItem("moveFile"));
      newFolder.id_carpeta_nueva = folder.id_carpeta;
      localStorage.setItem("cortarPegar", this.cortarPegar);
      localStorage.setItem("moveFile", JSON.stringify({ id_archivo: "", id_carpeta: "", id_usuario: "", id_carpeta_nueva: "" }));
      newFolder.ruta = "movfile";
      console.log(newFolder);

      await this.archivoService.moveFile(newFolder).toPromise().then(
        res => {
          console.log(res);
          const contenido = {
            position: 'center',
            title: "El archivo ha sido movido con exito",
            showConfirmButton: false,
            timer: 3000
          }
          this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
          this.ServicePeticiones.actualizarPagina();
          this.ServicePeticiones.actualizarPagina();
        },
        err => {
          console.error(err);
        }
      )
    }
  }

  async CrearCarpeta() {
    const inputValue = ""
    const { value: NameFolder } = await Swal.fire({
      title: 'NUEVA CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta',
      inputValue: inputValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (NameFolder) {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      var dataroot = {
        "ruta": "root",
        "id_user": user.id_user
      };
      this.ServicePeticiones.postRootFolder(dataroot)
        .subscribe(res => {
          let respuestaServer;
          respuestaServer = res;
          const idpadre = respuestaServer.carpetas[0].id;

          var inf_folder = {
            "ruta": "mkdir",
            "id_user": user.id_user,
            "fecha_creacion": Date.now(),
            "nombre": NameFolder,
            "id_padre": idpadre,
          };

          this.PostCreacionCarpeta(inf_folder);
        });
    }
  }

  PostCreacionCarpeta(datos: any) {
    let respuestaServer;
    this.ServicePeticiones.postCreateFolder(datos)
      .subscribe(res => {
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          const contenido = {
            position: 'center',
            title: 'La carpeta: ' + datos.nombre + " ha sido creada con exito",
            showConfirmButton: false,
            timer: 2000
          }
          /*Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'La carpeta: ' + datos.nombre + " ha sido creada con exito",
            showConfirmButton: false,
            timer: 2000
          })*/
          this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          const contenido = {
            title: 'Error al crear carpeta',
            text: respuestaServer.message,
          }
          this.tarjetaDesplegable.fabricaTarjeta(false,contenido);
          this.ServicePeticiones.actualizarPagina();
        }

      });
  }

  async ActualizarCarpeta() {
    const inputoldValue = ""
    const { value: oldFolder } = await Swal.fire({
      title: 'EDITAR CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta a editar',
      inputValue: inputoldValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (oldFolder) {
      const inputnewValue = ""
      const { value: newFolder } = await Swal.fire({
        title: 'EDITAR CARPETA',
        input: 'text',
        inputLabel: 'Ingrese el nuevo nombre de la carpeta',
        inputValue: inputnewValue,
        showCancelButton: true,
        inputValidator: (value) => {
          if (!value) {
            return 'You need to write something!'
          }
        }
      })

      if (newFolder) {
        const user = JSON.parse(localStorage.getItem('currentUser'));
        var dataroot = {
          "ruta": "root",
          "id_user": user.id_user
        };
        this.ServicePeticiones.postRootFolder(dataroot)
          .subscribe(res => {
            let respuestaServer;
            respuestaServer = res;
            const carpetas: Array<string> = respuestaServer.carpetas[0].carpetas;
            let existecarpeta = false;
            let idcarpetaeliminar = null;
            for (let index = 0; index < carpetas.length; index++) {
              const carpetaactual = JSON.parse(JSON.stringify(carpetas[index]));
              if (oldFolder == carpetaactual.nombre) {
                existecarpeta = true;
                idcarpetaeliminar = carpetaactual.id;
                break;
              }
            }

            if (existecarpeta) {
              var datafolder = {
                "ruta": "mvdir",
                "id_user": user.id_user,
                "id_carpeta": idcarpetaeliminar,
                "nombre": newFolder
              };

              this.updateFolder(datafolder);
            } else {
              const contenido = {
                title: 'Error al eliminar carpeta',
                text: "La carpeta: " + oldFolder + " NO EXISTE",
              }
              this.tarjetaDesplegable.fabricaTarjeta(false,contenido);
              this.ServicePeticiones.actualizarPagina();
            }
          });
      }
    }
  }

  updateFolder(datafolder: any) {
    this.ServicePeticiones.updateFolder(datafolder)
      .subscribe(res => {
        let respuestaServer;
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          const contenido = {
            position: 'center',
            title: "La carpeta ha sido actualizada con exito",
            showConfirmButton: false,
            timer: 2000
          }
          this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
          this.ServicePeticiones.actualizarPagina();
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          const contenido = {
            title: 'Error al editar la carpeta',
            text: respuestaServer.message,
          }
          this.tarjetaDesplegable.fabricaTarjeta(false,contenido);
          this.ServicePeticiones.actualizarPagina();
        }
      });
  }

  async EliminarCarpeta() {
    const inputValue = ""
    const { value: NameFolder } = await Swal.fire({
      title: 'ELIMINAR CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta',
      inputValue: inputValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (NameFolder) {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      var dataroot = {
        "ruta": "root",
        "id_user": user.id_user
      };
      this.ServicePeticiones.postRootFolder(dataroot)
        .subscribe(res => {
          let respuestaServer;
          respuestaServer = res;
          const carpetas: Array<string> = respuestaServer.carpetas[0].carpetas;
          let existecarpeta = false;
          let idcarpetaeliminar = null;
          for (let index = 0; index < carpetas.length; index++) {
            const carpetaactual = JSON.parse(JSON.stringify(carpetas[index]));
            if (NameFolder == carpetaactual.nombre) {
              existecarpeta = true;
              idcarpetaeliminar = carpetaactual.id;
              break;
            }
          }

          if (existecarpeta) {
            var datafolder = {
              "ruta": "rmdir",
              "id_user": user.id_user,
              "id_carpeta": idcarpetaeliminar
            };
            this.deleteFolder(datafolder);
          } else {
            const contenido = {
              title: 'Error al eliminar carpeta',
              text: "La carpeta: " + NameFolder + " NO EXISTE",
            }
            this.tarjetaDesplegable.fabricaTarjeta(false,contenido);
            this.ServicePeticiones.actualizarPagina();
          }
        });
    }
  }

  deleteFolder(datafolder: any) {
    this.ServicePeticiones.deleteFolder(datafolder)
      .subscribe(res => {
        let respuestaServer;
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          const contenido = {
            position: 'center',
            title: "La carpeta ha sido eliminada con exito",
            showConfirmButton: false,
            timer: 2000
          }
          this.tarjetaDesplegable.fabricaTarjeta(true,contenido);
          this.ServicePeticiones.actualizarPagina();
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          const contenido = {
            title: 'Error al crear carpeta',
            text: respuestaServer.message,
          }
          this.tarjetaDesplegable.fabricaTarjeta(false,contenido);
          this.ServicePeticiones.actualizarPagina();
        }
      });
  }

}
