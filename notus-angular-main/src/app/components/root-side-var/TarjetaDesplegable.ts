import Swal from 'sweetalert2'

export class TarjetaDesplegable{
    constructor(){}

    fabricaTarjeta(estado: boolean, contenido: any){
        if(estado){

            const msg: any = {
                position: contenido.position,
                title: contenido.title,
                showConfirmButton: false,
                timer: contenido.timer,
                icon: "success"
              };

            return Swal.fire(msg)
        }
        else{

            const msg: any = {
                title: contenido.title,
                text: contenido.text,
                icon: "error"
              }

            return Swal.fire(msg)
        }
    }
}