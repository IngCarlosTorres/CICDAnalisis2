import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArchivoService } from '../../services/archivo.service'
import { PeticionesService } from '../../services/peticiones.service';

import Swal from 'sweetalert2'
import { UsuarioService } from '../../services/usuario.service';


@Component({
  selector: 'app-folder-side-var',
  templateUrl: './folder-side-var.component.html'
})
export class FolderSideVarComponent implements OnInit {

  collapseShow = "hidden";
  cortarPegar = "";

  constructor(private router: Router,private ServicePeticiones: PeticionesService, private archivoService: ArchivoService,
    private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    if (localStorage.getItem("cortarPegar") == null) {
      localStorage.setItem("cortarPegar", "cortar");
    }
    this.cortarPegar = localStorage.getItem("cortarPegar");
  }

  toggleCollapseShow(classes) {
    this.collapseShow = classes;
  }

  async CrearCarpeta() {
    const inputValue = ""
    const { value: NameFolder } = await Swal.fire({
      title: 'NUEVA CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta',
      inputValue: inputValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (NameFolder) {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      var dataroot = {
        "ruta": "root",
        "id_user": user.id_user
      };
      await this.usuarioService.getSelectedFolder().toPromise().then(res => {
          let respuestaServer;
          respuestaServer = res;
          const idpadre = respuestaServer.carpetas.id;

          var inf_folder = {
            "ruta": "mkdir",
            "id_user": user.id_user,
            "fecha_creacion": Date.now(),
            "nombre": NameFolder,
            "id_padre": idpadre,
          };

          this.PostCreacionCarpeta(inf_folder);
        });
    }
  }

  PostCreacionCarpeta(datos: any) {
    let respuestaServer;
    this.ServicePeticiones.postCreateFolder(datos)
      .subscribe(res => {
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'La carpeta: ' + datos.nombre + " ha sido creada con exito",
            showConfirmButton: false,
            timer: 2000
          })
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          Swal.fire({
            icon: 'error',
            title: 'Error al crear carpeta',
            text: respuestaServer.message,
          })
        }

      });
  }

  async ActualizarCarpeta() {
    const inputoldValue = ""
    const { value: oldFolder } = await Swal.fire({
      title: 'EDITAR CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta a editar',
      inputValue: inputoldValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (oldFolder) {
      const inputnewValue = ""
      const { value: newFolder } = await Swal.fire({
        title: 'EDITAR CARPETA',
        input: 'text',
        inputLabel: 'Ingrese el nuevo nombre de la carpeta',
        inputValue: inputnewValue,
        showCancelButton: true,
        inputValidator: (value) => {
          if (!value) {
            return 'You need to write something!'
          }
        }
      })

      if (newFolder) {
        const user = JSON.parse(localStorage.getItem('currentUser'));
        var dataroot = {
          "ruta": "root",
          "id_user": user.id_user
        };
        await this.usuarioService.getSelectedFolder().toPromise().then(res => {
            let respuestaServer;
            respuestaServer = res;
            const carpetas: Array<string> = respuestaServer.carpetas.carpetas;
            let existecarpeta = false;
            let idcarpetaeliminar = null;
            for (let index = 0; index < carpetas.length; index++) {
              const carpetaactual = JSON.parse(JSON.stringify(carpetas[index]));
              if (oldFolder == carpetaactual.nombre) {
                existecarpeta = true;
                idcarpetaeliminar = carpetaactual.id;
                break;
              }
            }

            if (existecarpeta) {
              var datafolder = {
                "ruta": "mvdir",
                "id_user": user.id_user,
                "id_carpeta": idcarpetaeliminar,
                "nombre": newFolder
              };

              this.updateFolder(datafolder);
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Error al eliminar carpeta',
                text: "La carpeta: " + oldFolder + " NO EXISTE",
              })
            }
          });
      }
    }
  }

  updateFolder(datafolder: any) {
    this.ServicePeticiones.updateFolder(datafolder)
      .subscribe(res => {
        let respuestaServer;
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: "La carpeta ha sido actualizada con exito",
            showConfirmButton: false,
            timer: 2000
          })
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          Swal.fire({
            icon: 'error',
            title: 'Error al editar la carpeta',
            text: respuestaServer.message,
          })
        }
      });
  }

  async EliminarCarpeta() {
    const inputValue = ""
    const { value: NameFolder } = await Swal.fire({
      title: 'ELIMINAR CARPETA',
      input: 'text',
      inputLabel: 'Ingrese el nombre de la carpeta',
      inputValue: inputValue,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'You need to write something!'
        }
      }
    })

    if (NameFolder) {
      const user = JSON.parse(localStorage.getItem('currentUser'));
      var dataroot = {
        "ruta": "root",
        "id_user": user.id_user
      };
      await this.usuarioService.getSelectedFolder().toPromise().then(res => {
          let respuestaServer;
          respuestaServer = res;
          const carpetas: Array<string> = respuestaServer.carpetas.carpetas;
          let existecarpeta = false;
          let idcarpetaeliminar = null;
          for (let index = 0; index < carpetas.length; index++) {
            const carpetaactual = JSON.parse(JSON.stringify(carpetas[index]));
            if (NameFolder == carpetaactual.nombre) {
              existecarpeta = true;
              idcarpetaeliminar = carpetaactual.id;
              break;
            }
          }

          if (existecarpeta) {
            var datafolder = {
              "ruta": "rmdir",
              "id_user": user.id_user,
              "id_carpeta": idcarpetaeliminar
            };
            this.deleteFolder(datafolder);
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Error al eliminar carpeta',
              text: "La carpeta: " + NameFolder + " NO EXISTE",
            })
          }
        });
    }
  }

  deleteFolder(datafolder: any) {
    this.ServicePeticiones.deleteFolder(datafolder)
      .subscribe(res => {
        let respuestaServer;
        respuestaServer = res;
        if (respuestaServer.estado == 200) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: "La carpeta ha sido eliminada con exito",
            showConfirmButton: false,
            timer: 2000
          })
          this.ServicePeticiones.actualizarPagina();
        }
        else {
          Swal.fire({
            icon: 'error',
            title: 'Error al crear carpeta',
            text: respuestaServer.message,
          })
        }
      });
  }

  async deleteFile() {
    const file = JSON.parse(localStorage.getItem("currentFile"));
    file.ruta = "rmfile";

    await this.archivoService.deleteFile(file).toPromise().then(
      res => {
        localStorage.setItem('currentFile', "");
        window.location.reload();
      },
      err => {
        console.error(err);
      }
    );
  }

  async moveFile() {
    if (this.cortarPegar == "cortar") {
      this.cortarPegar = "pegar";
      const file = JSON.parse(localStorage.getItem("currentFile"));
      file.id_carpeta_nueva = "";
      localStorage.setItem("cortarPegar", this.cortarPegar);
      localStorage.setItem("moveFile", JSON.stringify(file));
    }
    else if (this.cortarPegar == "pegar") {
      this.cortarPegar = "cortar";
      const folder = JSON.parse(localStorage.getItem("currentFolder"));
      const newFolder = JSON.parse(localStorage.getItem("moveFile"));
      newFolder.id_carpeta_nueva = folder.id_carpeta;
      localStorage.setItem("cortarPegar", this.cortarPegar);
      localStorage.setItem("moveFile", JSON.stringify({ id_archivo: "", id_carpeta: "", id_usuario: "", id_carpeta_nueva: "" }));
      newFolder.ruta = "movfile";

      await this.archivoService.moveFile(newFolder).toPromise().then(
        res => {
          console.log(res);
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: "El archivo ha sido movido con exito",
            showConfirmButton: false,
            timer: 3000
          })
          this.archivoService.actualizarPagina();
        },
        err => {
          console.error(err);
        }
      )
     
    }
  }

  async moverPapelera() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const file = JSON.parse(localStorage.getItem("currentFile"));

    console.log("id de papelera: " + user.id_papelera);
    var dataroot = {
      ruta: "papelera",
      id_user: user.id_user,
      id_carpeta: file.id_carpeta,
      id_archivo: file.id_archivo,
      id_carpeta_nueva: user.id_papelera

    };
    //console.log(dataroot)
    await this.archivoService.moveFile(dataroot).toPromise().then(
      res => {
        console.log(res);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: "El archivo ha sido enviado a la papelera con exito",
          showConfirmButton: false,
          timer: 3000
        })
        localStorage.setItem("oldCarpeta", file.id_carpeta);

        this.ServicePeticiones.actualizarPagina();
        localStorage.setItem("oldCarpeta", file.id_carpeta);

      },
      err => {
        console.error(err);
      }
    )

  }

  
  async enviar(){
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const file = JSON.parse(localStorage.getItem("currentFile"));

    var dataroot = {
      ruta: "enviar",
      id_user: user.id_user,
      id_carpeta: file.id_carpeta,
      id_archivo: file.id_archivo,
      correo: user.correo
    };

    console.log(dataroot);
    await this.archivoService.enviar(dataroot).toPromise().then(
      res => {
        console.log(res);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: "El archivo ha sido enviado a tu correo con exito",
          showConfirmButton: false,
          timer: 3000
        })
        this.ServicePeticiones.actualizarPagina();

      },
      err => {
        console.error(err);
      }
    )

  }

}


