import { Component, OnInit } from '@angular/core';
import { browser } from 'protractor';
import { UsuarioService } from '../../services/usuario.service'
import { ArchivoService } from '../../services/archivo.service'
import { Router } from '@angular/router';
import { PeticionesService } from '../../services/peticiones.service';

@Component({
  selector: 'app-carpeta',
  templateUrl: './carpeta.component.html'
})
export class CarpetaComponent implements OnInit {

  filesFolder = [];
  carpetaActual = null;
  archivoSeleccionado = null;
  carpetasUsuario = [];
  userFolder = null;
  carpetaSeleccionada = null;

  constructor(private usuarioService: UsuarioService, private archivoService: ArchivoService, private router: Router,
    private ServicePeticiones: PeticionesService) { }

  ngOnInit(): void {
    this.getFolderContent();
    
  }

  async getFolderContent(){
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.userFolder = user;
    await this.usuarioService.getSelectedFolder().subscribe(
      (res: any) => {
        this.carpetaActual = res.carpetas;
        console.log(this.carpetaActual)
        let tempDate: Date = null;
        (this.carpetaActual.archivos as []).forEach((archivo: any) => {
          tempDate = new Date(archivo.fecha_subida);
          archivo.fecha_subida = `${tempDate.getDate()} / ${tempDate.getMonth()} / ${tempDate.getFullYear()}`;
          archivo.iconColor = "bg-red-500";

          this.filesFolder.push(archivo);
        });

        (this.carpetaActual.carpetas as []).forEach((carpeta: any) => {
          tempDate = new Date(carpeta.fecha_creacion);
          carpeta.fecha_creacion = `${tempDate.getDate()} / ${tempDate.getMonth()} / ${tempDate.getFullYear()}`;
          carpeta.iconColor = "bg-emerald-500";

          this.carpetasUsuario.push(carpeta);
        });
      },
      err => console.error(err)
    )
  }

  selectFile(archivo: any){

    const tempFolder = this.archivoService.getSelectedFolderUser();
    this.archivoService.setSelectedFile(tempFolder.id_user,tempFolder.id_carpeta,archivo.id_archivo);

    if(this.archivoSeleccionado && archivo.id_archivo != this.archivoSeleccionado.id_archivo){
      this.archivoSeleccionado.iconColor = "bg-red-500";
    }

    archivo.iconColor = archivo.iconColor == "bg-red-500" ? "bg-indigo-500" : "bg-red-500";
    this.archivoSeleccionado = archivo;
  }

  getIntoFile(file){
     if(file.extension == "jpg" || file.extension == "jpeg" || file.extension == "png" || file.extension == "gif" || file.extension == "svg" ||
        file.extension == "mp4" || file.extension == "avi" || file.extension == "flv" || file.extension == "mov" || file.extension == "txt"){
      window.open(file.link,"_blank");
     }
     else{
      this.archivoService.downloadFile(file.link).subscribe(archivo => {

        //Creamos un archivo blob con nuestro archivo en S3
        var newBlob = new Blob([archivo], { type: `application/${file.extension}`});
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob, file.nombre_archivo);
          return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = file.nombre_archivo;
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
  
       });
     }
  }

  selectFolder(carpeta: any){

    if(this.carpetaSeleccionada && carpeta.id != this.carpetaSeleccionada.id){
      this.carpetaSeleccionada.iconColor = "bg-emerald-500";
    }

    carpeta.iconColor = carpeta.iconColor == "bg-emerald-500" ? "bg-orange-500" : "bg-emerald-500";
    this.carpetaSeleccionada = carpeta;
  }

  getIntoFolder(idCarpeta){
    const user = JSON.parse(localStorage.getItem('currentUser'));
   

    if (idCarpeta == user.id_papelera){
      this.usuarioService.setSelectedFolder(this.userFolder.id_user,idCarpeta)
      this.router.navigate(['/user/papelera'])


    }else{
      this.usuarioService.setSelectedFolder(this.userFolder.id_user,idCarpeta)
      this.ServicePeticiones.actualizarPagina();

    }


  }

}
