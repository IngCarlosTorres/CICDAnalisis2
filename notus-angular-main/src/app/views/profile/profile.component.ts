import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service'


import Swal from 'sweetalert2'


import { PeticionesService } from '../../services/peticiones.service';
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
})
export class ProfileComponent implements OnInit {

  public usuario = {
    nickname: '',
    nombre: '',
    numero: '',
    correo: '',
    fecha: '',
    contrasena: '',
    contrasena2: '',
    fa_correo: false,
    fa_mensaje: false
  };

  userActual = "";
  infuser = "";
  user2 = null;
  public showAlert = false;
  public messageAlert = '';

  constructor(public ServicePeticiones: PeticionesService, private router: Router, private usuarioService: UsuarioService) {

  }

  ngOnInit(): void {
    this.usuario.nombre = this.ServicePeticiones.Obtenernombre();
    this.usuario.correo = this.ServicePeticiones.ObtenerCorreo();
    this.usuario.numero = this.ServicePeticiones.Obtenernumero();
    this.usuario.fecha = this.ServicePeticiones.Obtenerfecha();
    this.usuario.nickname = this.ServicePeticiones.ObtenerUser();
    this.usuario.contrasena = this.ServicePeticiones.Obtenercontrasena();
    this.usuario.fa_correo = this.ServicePeticiones.Obtenerauth().fa_correo;
    this.usuario.fa_mensaje = this.ServicePeticiones.Obtenerauth().fa_mensaje;

    this.userActual = "";
    this.userActual = this.ServicePeticiones.ObtenerUser();
    if (this.userActual != "") {
      //Aqui tiene que ir el conexion con el edit

    }
    else {
      this.router.navigate(['/auth/restringido']);
    }
    
    

  }

  async edit(){
    //if (this.usuario.contrasena == this.usuario.contrasena2){
      const user = JSON.parse(localStorage.getItem('currentUser'));
      this.user2 = user;
      let tipo_not = -1;

      if(this.usuario.fa_correo){
        tipo_not = 0;
      }

      if (this.usuario.fa_mensaje){
        tipo_not = 1;
      }

      if(this.usuario.fa_correo && this.usuario.fa_mensaje){
        tipo_not = 2;
      }
      
      const infuser = {
        "ruta": "editar",
        "id_user": user.id_user,
        "nombre": this.usuario.nombre,
        "numero": this.usuario.numero,
        "correo": this.usuario.correo,
        "fecha_nacimiento": this.usuario.fecha,
        "contrasena": this.usuario.contrasena,
        "auth_val": (this.usuario.fa_correo || this.usuario.fa_mensaje),
        "tipo_notificacion": tipo_not 
      }
      
      const infuser2 = {
        usuario: this.usuario.nickname,
        id_user: user.id_user,
        numero: this.usuario.numero,
        nombre: this.usuario.nombre,
        correo: this.usuario.correo,
        fecha: this.usuario.fecha,
        contrasena: this.usuario.contrasena,
      }

      let respuestaServer;
      await this.usuarioService.editarUsuario(infuser).subscribe(res => {
        respuestaServer = res;
        if(respuestaServer.estado != 200){
          Swal.fire({
            icon: 'error',
            title: 'Credenciales Incorrectas',
            text: 'Las credenciales ingresadas no existen o estan incorrectas, vuelva a intentarlo'
          })
          
        }else{
          Swal.fire({
            title: 'Usuario Editado correctamente',
            width: 600,
            padding: '3em',
            background: '#fff url(/assets/img/trees.png)',
            backdrop: `
              rgba(0,0,123,0.4)
              url("/assets/img//confetti.gif")
              center top
              no-repeat
            `
          }) 
          localStorage.setItem('currentUser', JSON.stringify(infuser2));
          this.router.navigate(["/user/miunidad"]);

        }
      })
    /*}else {
      Swal.fire({
        icon: 'error',
        title: 'Contraseñas diferentes',
        text: 'Las contraseñas no coinciden.'
      })
    }*/
  }
  


}
