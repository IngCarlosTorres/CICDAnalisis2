import { Component, OnInit } from '@angular/core';
import { ArchivoService } from '../../services/archivo.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-subir-archivo',
  templateUrl: './subir-archivo.component.html'
})
export class SubirArchivoComponent implements OnInit {

  nuevoArchivo: any = {
    ruta: "cat",
    ext: "",
    archivo: "",
    nombre_archivo: "",
    id_user: "",
    id_carpeta: ""
  };

  constructor(private archivoService: ArchivoService, private location: Location) { }

  ngOnInit(): void {
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];

    if (file.type.split('/')[1] == "plain" ){
      this.nuevoArchivo.ext = "txt"
    } else{
      this.nuevoArchivo.ext = file.type.split('/')[1];
    }
    console.log(this.nuevoArchivo.ext)

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        this.nuevoArchivo.archivo = (reader.result as string).split('base64,')[1];
    };

  }

  async createFile(){
    const folder = this.archivoService.getSelectedFolderUser();
    this.nuevoArchivo.id_user = folder.id_user;
    this.nuevoArchivo.id_carpeta = folder.id_carpeta;

    console.log(this.nuevoArchivo);

    await this.archivoService.createFile(this.nuevoArchivo).toPromise().then(
      res => {
        this.location.back();
      },
      err => {
        console.error(err);
      }
    )

  }

}
