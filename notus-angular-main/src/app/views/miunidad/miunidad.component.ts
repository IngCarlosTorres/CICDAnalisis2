import { Component, OnInit } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';
import { ArchivoService } from '../../services/archivo.service';

import Swal from 'sweetalert2'

// IMPORTACIÓN DEL SERVICIO PETICIONES
import{PeticionesService} from '../../services/peticiones.service';

@Component({
  selector: 'app-miunidad',
  templateUrl: './miunidad.component.html'
})
export class MiunidadComponent implements OnInit {
  userActual = "";
  carpetaSeleccionada = null;
  carpetasUsuario = [];
  userFolder = null;

  filesFolder = [];
  carpetaActual = null;
  archivoSeleccionado = null;
  
  constructor(public ServicePeticiones: PeticionesService,private router: Router, private usuarioService: UsuarioService, 
    private archivoService: ArchivoService) { }

  async ngOnInit() {
    this.userActual="";
    this.userActual=this.ServicePeticiones.ObtenerUser();
    if(this.userActual!=""){

    }
    else{
      this.router.navigate(['/auth/restringido']);
    }
    await this.listarCarpetas();
    await this.getFolderContent();
  }

  async listarCarpetas() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.userFolder = user;
    const root = {
        "ruta": "root",
        "id_user": user.id_user
      }

    await this.usuarioService.getFoldersUser(root).subscribe(
      (res: any) => {
        //this.carpetasUsuario = res.carpetas[0].carpetas;
        this.usuarioService.setSelectedFolder(this.userFolder.id_user,res.carpetas[0].id)

        let tempDate: Date = null;
        (res.carpetas[0].carpetas as []).forEach( (carpeta: any) => {
          tempDate = new Date(carpeta.fecha_creacion);
          carpeta.fecha_creacion = `${tempDate.getDate()} / ${tempDate.getMonth()} / ${tempDate.getFullYear()}`;
          carpeta.iconColor = "bg-emerald-500";

          this.carpetasUsuario.push(carpeta);
        })
        
      },
      err => {
        console.error(err)
      }
    );
  }

  selectFolder(carpeta: any){

    if(this.carpetaSeleccionada && carpeta.id != this.carpetaSeleccionada.id){
      this.carpetaSeleccionada.iconColor = "bg-emerald-500";
    }

    carpeta.iconColor = carpeta.iconColor == "bg-emerald-500" ? "bg-orange-500" : "bg-emerald-500";
    this.carpetaSeleccionada = carpeta;
  }

  getIntoFolder(idCarpeta){
    const user = JSON.parse(localStorage.getItem('currentUser'));
   

    if (idCarpeta == user.id_papelera){
      this.usuarioService.setSelectedFolder(this.userFolder.id_user,idCarpeta)
      console.log("....... prueba entrar a papelera.........")
      this.router.navigate(['/user/papelera'])


    }else{
      this.usuarioService.setSelectedFolder(this.userFolder.id_user,idCarpeta)
      console.log("....... prueba no entro a papelera.........")

      this.router.navigate(['/user/carpeta'])

    }


  }

  async getFolderContent(){
    await this.usuarioService.getSelectedFolder().subscribe(
      (res: any) => {
        this.carpetaActual = res.carpetas;

        let tempDate: Date = null;
        (this.carpetaActual.archivos as []).forEach((archivo: any) => {
          tempDate = new Date(archivo.fecha_subida);
          archivo.fecha_subida = `${tempDate.getDate()} / ${tempDate.getMonth()} / ${tempDate.getFullYear()}`;
          archivo.iconColor = "bg-red-500";

          this.filesFolder.push(archivo);
        });
      },
      err => console.error(err)
    )
  }

  selectFile(archivo: any){

    const tempFolder = this.archivoService.getSelectedFolderUser();
    this.archivoService.setSelectedFile(tempFolder.id_user,tempFolder.id_carpeta,archivo.id_archivo);

    if(this.archivoSeleccionado && archivo.id_archivo != this.archivoSeleccionado.id_archivo){
      this.archivoSeleccionado.iconColor = "bg-red-500";
    }

    archivo.iconColor = archivo.iconColor == "bg-red-500" ? "bg-indigo-500" : "bg-red-500";
    this.archivoSeleccionado = archivo;
  }

  getIntoFile(file){
     if(file.extension == "jpg" || file.extension == "jpeg" || file.extension == "png" || file.extension == "gif" || file.extension == "svg" ||
        file.extension == "mp4" || file.extension == "avi" || file.extension == "flv" || file.extension == "mov" || file.extension == "txt"){
      window.open(file.link,"_blank");
     }
     else{
      this.archivoService.downloadFile(file.link).subscribe(archivo => {

        //Creamos un archivo blob con nuestro archivo en S3
        var newBlob = new Blob([archivo], { type: `application/${file.extension}`});
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob, file.nombre_archivo);
          return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = file.nombre_archivo;
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
  
       });
     }
  }

}
